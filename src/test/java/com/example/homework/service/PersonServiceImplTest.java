package com.example.homework.service;

import com.example.homework.dao.PersonRepository;
import com.example.homework.models.Person;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
class PersonServiceImplTest {

    @InjectMocks
    PersonServiceImpl personService;

    @Mock
    PersonRepository personRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getSpecificPerson() {
        Person testPerson1 = new Person("Marcin", "Dolicher", 1);
        Person testPerson2 = new Person("Anna", "Kowalska", 5);
        Person testPerson3 = new Person("Marek", "Driver", 21);
        Person testPerson4 = new Person("Antek", "Kaczmarek", 19);
        List<Person> it_person = new ArrayList<>() {{
            add(testPerson1);
            add(testPerson2);
            add(testPerson3);
            add(testPerson4);
        }};
        when(personRepository.findAll()).thenReturn(it_person);

        List<Person> result = (List<Person>) personService.getSpecificPerson();

        assertEquals(testPerson2, result.get(0));
        assertEquals(testPerson4, result.get(1));
    }

    @Test
    void addPerson() {
    }

    @Test
    void getLastAddedPerson() {
    }
}