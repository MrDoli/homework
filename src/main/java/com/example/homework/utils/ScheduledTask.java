package com.example.homework.utils;

import com.example.homework.dao.PersonRepository;
import com.example.homework.models.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTask {

    private static final Logger logger = LoggerFactory.getLogger(ScheduledTask.class);

    private final PersonRepository personRepository;

    public ScheduledTask(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    // 5 min = 300000 ms
    @Scheduled(fixedRate = 300000)
    public void reportCurrentTime() {
        Person person = personRepository.findTopByOrderByIdDesc();
        logger.info("Value of last before adding: " + person.getLast());
        person.addToLast(10);
        personRepository.save(person);
        logger.info("Value of last after adding: " + person.getLast());
    }
}
