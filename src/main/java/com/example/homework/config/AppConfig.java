package com.example.homework.config;

import java.util.Date;

public class AppConfig {
	private int maxPersonNumber;
	private Date initalizationTimestamp;

	public AppConfig(int maxPersonNumber, Date initalizationTimestamp) {
		super();
		this.maxPersonNumber = maxPersonNumber;
		this.initalizationTimestamp = initalizationTimestamp;
	}

	public int getMaxPersonNumber() {
		return maxPersonNumber;
	}

	public void setMaxPersonNumber(int maxPersonNumber) {
		this.maxPersonNumber = maxPersonNumber;
	}

	public Date getInitalizationTimestamp() {
		return initalizationTimestamp;
	}

	public void setInitalizationTimestamp(Date initalizationTimestamp) {
		this.initalizationTimestamp = initalizationTimestamp;
	}

}
