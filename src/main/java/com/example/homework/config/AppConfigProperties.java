package com.example.homework.config;

import com.sun.istack.NotNull;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import java.util.Date;

@Configuration
@Validated
public class AppConfigProperties {

    @NotNull
    private int maxPersonNumber;

    @NotNull
    private Date initalizationTimestamp;

    public int getMaxPersonNumber() {
        return maxPersonNumber;
    }

    public void setMaxPersonNumber(int maxPersonNumber) {
        this.maxPersonNumber = maxPersonNumber;
    }

    public Date getInitalizationTimestamp() {
        return initalizationTimestamp;
    }

    public void setInitalizationTimestamp(Date initalizationTimestamp) {
        this.initalizationTimestamp = initalizationTimestamp;
    }

    @Bean
    @ConfigurationProperties(prefix = "app")
    public AppConfig appConfig() {
        return new AppConfig(getMaxPersonNumber(), getInitalizationTimestamp());
    }
}
