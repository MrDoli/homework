package com.example.homework.dao;

import com.example.homework.models.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Integer> {
    Person findTopByOrderByIdDesc();
}
