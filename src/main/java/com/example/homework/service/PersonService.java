package com.example.homework.service;

import com.example.homework.models.Person;
import org.springframework.validation.annotation.Validated;

@Validated
public interface PersonService {

    Iterable<Person> getSpecificPerson();

    Person addPerson(String firstName, String lastName, int last);

    Person getLastAddedPerson();

}
