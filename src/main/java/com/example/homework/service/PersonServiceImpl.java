package com.example.homework.service;

import com.example.homework.dao.PersonRepository;
import com.example.homework.models.Person;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Spliterator;
import java.util.stream.StreamSupport;

@Service
public class PersonServiceImpl implements PersonService {

    @Value("${feature.second-letter-enabled}")
    private boolean secondLetterEnabled;

    private final PersonRepository personRepository;

    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public Iterable<Person> getSpecificPerson() {
        Iterable<Person> persons = personRepository.findAll();
        Spliterator<Person> spliterator = persons.spliterator();
        List<Person> results = new ArrayList<>();
        StreamSupport.stream(spliterator, false)
                .filter(n -> n.getFirstName().charAt(1) == getCorrectCharForEnviroment())
                .forEach(results::add);
        return results;
    }

    @Override
    public Person addPerson(String firstName, String lastName, int last) {
        Person person = new Person(firstName, lastName, last);
        return this.personRepository.save(person);
    }

    @Override
    public Person getLastAddedPerson() {
        return this.personRepository.findTopByOrderByIdDesc();
    }

    private char getCorrectCharForEnviroment(){
        return (this.isSecondLetterEnabled()  ? 'l' : 'n');
    }

    public boolean isSecondLetterEnabled() {
        return secondLetterEnabled;
    }

    public void setSecondLetterEnabled(boolean secondLetterEnabled) {
        this.secondLetterEnabled = secondLetterEnabled;
    }
}
