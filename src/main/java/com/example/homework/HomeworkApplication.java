package com.example.homework;

import com.example.homework.config.AppConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

@SpringBootApplication
@EnableScheduling
public class HomeworkApplication {

    private static final Logger logger = LoggerFactory.getLogger(HomeworkApplication.class);

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(HomeworkApplication.class, args);
        AppConfig appConfig = context.getBean(AppConfig.class);

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        logger.info("Max Person Number: " + Integer.toString(appConfig.getMaxPersonNumber()));
        logger.info("Date: " + df.format(appConfig.getInitalizationTimestamp()));
    }

}
