package com.example.homework.controller;

import com.example.homework.models.Person;
import com.example.homework.service.PersonService;
import org.springframework.web.bind.annotation.*;

@RestController
public class PersonController {

    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/specific-person")
    public @ResponseBody Iterable<Person> getSpecificPerson() {
        return personService.getSpecificPerson();
    }

    @PostMapping("/add")
    public @ResponseBody String addPerson(@RequestParam String firstName, @RequestParam String lastName, @RequestParam int last) {
        personService.addPerson(firstName, lastName, last);
        return "Added new person to repository!";
    }

    @GetMapping("/last-added-person")
    public @ResponseBody Person getLastAddedPerson() {
        return personService.getLastAddedPerson();
    }
}
